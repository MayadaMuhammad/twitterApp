import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './twitterapp.html';

Template.tweetsOps.onCreated(function() {
});

Template.tweetsOps.helpers({
	tweets: function(){
		return Posts.find({}, { sort: { createdAt: -1 }, count: 10 });
	},

	tweetsstatus: function(status){
		statuses = ['Fav', 'Mark', 'Done', 'Reply', 'Read later'];
		const index = statuses.indexOf(status);
        statuses.splice(index, 1);
        return statuses;
    }
});

Template.tweetsOps.events = {
    'keydown input#keyword' : function (event, instance) {
	    if (event.which == 13) { // 13 is the enter key event
        	var keyword = document.getElementById('keyword');
        	if (keyword.value != '') {
      			Meteor.call('twitterSearch', keyword.value);
	        	document.getElementById('keyword').value = '';
        	}
	    }
    },

    'keydown input#tweet' : function (event) {
	    if (event.which == 13) { // 13 is the enter key event
        	var tweet = document.getElementById('tweet');
        	if (tweet.value != '') {
      			Meteor.call('postTwitter', tweet.value);
	        	document.getElementById('tweet').value = '';
        	}
	    }
    },

    'click button#updateTweetStatus': function (event) {
    	var e = document.getElementById('tweetstatus');
        var status = e.options[e.selectedIndex].text;
        Meteor.call('statusChange', this._id, status);
    }
};